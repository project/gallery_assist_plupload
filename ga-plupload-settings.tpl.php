<?php
?>

Drupal.behaviors.ga_plupload_resize_params = function (context) {

	// Setup all version
	$("#ga-pluploader").pluploadQueue({
		// General settings
		runtimes : '<?php print $runtimes; ?>',
		url : '<?php print $url; ?>',
		max_file_size : '<?php print $max_file_size; ?>',
		chunk_size : '<?php print $chunk_size; ?>',
		unique_names : '<?php print $unique_names; ?>',
        flash_swf_url : '<?php print $flash_swf_url; ?>l',
		filters : [
			{title : "<?php print $filters['images']['title']; ?>", extensions : "<?php print $filters['images']['extensions']; ?>"},
			<?php if (isset($filters['archives'])) { ?>
            {title : "<?php print $filters['archives']['title']; ?>", extensions : "<?php print $filters['archives']['extensions']; ?>"}
            <?php } ?>
		],
        <?php if ($rename) { ?>
        rename : <?php print $rename; ?>,
        <?php } ?>
		// Resize images on clientside if we can
		<?php if ($resize) { ?>
        resize : {width : <?php print $resize_params['width']; ?>, height : <?php print $resize_params['height']; ?>, quality : <?php print $resize_params['quality']; ?>}
        <?php } ?>
	});

};