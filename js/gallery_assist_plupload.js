
Drupal.behaviors.ga_plupload_resize_params = function (context) {
  var resize_toggle = $('input#edit-plupload-resize');
  var resize_container = $('fieldset#resize-params-container');

  if (resize_toggle.attr('checked') == false) {
    resize_container.hide();
  }

  resize_toggle.change(function () {
    if (resize_toggle.attr('checked')) {
      resize_container.slideDown();
    }
    else {
      resize_container.slideUp();
    }
  });
}
